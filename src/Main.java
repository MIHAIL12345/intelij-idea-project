public class Main {public class Main {
    public static void main(String[] args) {
        // Използваме фабриките за създаване на различни видове яйца
        EggFactory chickenEggFactory = new ChickenEggFactory();
        EggFactory ostrichEggFactory = new OstrichEggFactory();
        EggFactory dinosaurEggFactory = new DinosaurEggFactory();

        // Създаваме яйца и ги декорираме
        Egg chickenEgg = new ColoredEggDecorator(chickenEggFactory.createEgg());
        Egg ostrichEgg = new StickeredEggDecorator(ostrichEggFactory.createEgg());
        Egg dinosaurEgg = dinosaurEggFactory.createEgg();

        // Избираме стратегия за скриване на яйцата и ги скриваме
        HidingStrategy basketStrategy = new BasketHidingStrategy();
        HidingStrategy forestStrategy = new ForestHidingStrategy();
        HidingStrategy bushStrategy = new BushHidingStrategy();

        basketStrategy.hide(chickenEgg);
        forestStrategy.hide(ostrichEgg);
        bushStrategy.hide(dinosaurEgg);
    }
}
}
