public class bunny {
    public class EasterBunny {
        private static EasterBunny instance;

        private EasterBunny() {
        }

        public static EasterBunny getInstance() {
            if (instance == null) {
                instance = new EasterBunny();
            }
            return instance;
        }

        public void hideEgg(Egg egg, String hidingPlace) {
            System.out.println("Hiding " + egg.getType() + " egg under " + hidingPlace);
        }
    }
}
