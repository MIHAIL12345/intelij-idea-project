public abstract class EggDecorator implements Egg {
    protected Egg decoratedEgg;

    public EggDecorator(Egg decoratedEgg) {
        this.decoratedEgg = decoratedEgg;
    }

    @Override
    public String getType() {
        return decoratedEgg.getType();
    }
}

public class ColoredEggDecorator extends EggDecorator {
    public ColoredEggDecorator(Egg decoratedEgg) {
        super(decoratedEgg);
    }

    @Override
    public String getType() {
        return "Colored " + decoratedEgg.getType() + " egg";
    }
}

public class StickeredEggDecorator extends EggDecorator {
    public StickeredEggDecorator(Egg decoratedEgg) {
        super(decoratedEgg);
    }

    @Override
    public String getType() {
        return "Stickered " + decoratedEgg.getType() + " egg";
    }
}