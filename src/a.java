public interface HidingStrategy {
    void hide(Egg egg);
}

public class BasketHidingStrategy implements HidingStrategy {
    @Override
    public void hide(Egg egg) {
        EasterBunny.getInstance().hideEgg(egg, "basket");
    }
}

public class ForestHidingStrategy implements HidingStrategy {
    @Override
    public void hide(Egg egg) {
        EasterBunny.getInstance().hideEgg(egg, "forest");
    }
}

public class BushHidingStrategy implements HidingStrategy {
    @Override
    public void hide(Egg egg) {
        EasterBunny.getInstance().hideEgg(egg, "bush");
    }
}