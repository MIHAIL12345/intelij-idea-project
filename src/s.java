public interface EggFactory {
    Egg createEgg();
}

public class ChickenEggFactory implements EggFactory {
    @Override
    public Egg createEgg() {
        return new ChickenEgg();
    }
}

public class OstrichEggFactory implements EggFactory {
    @Override
    public Egg createEgg() {
        return new OstrichEgg();
    }
}

public class DinosaurEggFactory implements EggFactory {
    @Override
    public Egg createEgg() {
        return new DinosaurEgg();
    }
}
